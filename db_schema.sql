create table users (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(255) UNIQUE,
  passwordDigest VARCHAR(255) NOT NULL,
  fullName VARCHAR(255) NOT NULL
);
