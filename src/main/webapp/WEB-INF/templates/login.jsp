<%@include file="_header.jsp"%>

<div class="error">${error}</div>

<form action="/login.html" method="post">
    <label for="username">Username: <input id="username" name="username" type="text" value="${loginForm.username}"></label>
    <br>
    <label for="password">Password: <input id="password" name="password" type="password"></label>
    <br>
    <button>Log In</button>
</form>

<%@include file="_footer.jsp"%>