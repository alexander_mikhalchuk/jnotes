<%@include file="_header.jsp"%>

<%@include file="_errorList.jsp"%>

<form action="/registration.html" method="post">
    <label for="username">Username: <input id="username" type="text" name="username" value="${registrationForm.username}"></label> <br>
    <label for="password">Password: <input id="password" type="password" name="password" value=""></label> <br>
    <label for="fullName">Full Name: <input id="fullName" type="text" name="fullName" value="${registrationForm.fullName}"></label> <br>
    <button type="submit">Register</button>
</form>

<%@include file="_footer.jsp"%>