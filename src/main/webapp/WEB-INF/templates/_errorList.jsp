<c:if test="${not empty errors}">
    <ul class="errors">
        <c:forEach var="error" items="${errors}">
            <li>${error}</li>
        </c:forEach>
    </ul>
</c:if>
