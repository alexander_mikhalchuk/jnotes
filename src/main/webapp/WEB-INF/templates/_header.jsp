<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/common.css">
</head>
<body>
<h2>JNotes</h2>

<ul class="main_menu">
    <li><a href="/index.html">JNotes</a></li>
    <c:if test="${!isLoggedIn}">
        <li><a href="/registration.html">Registration</a></li>
        <li><a href="/login.html">Log In</a></li>
    </c:if>
    <c:if test="${isLoggedIn}">
        <li><a href="/user/createNewNote.html">Create new note</a></li>
        <li><a href="/logout.html">Log Out</a></li>
        <li>Hi, ${userName}</li>
    </c:if>
</ul>
