package com.mykhalchuk.java.jnotes;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.List;

public class BindUtil {
    public static <T> T bind(HttpServletRequest req, Class<T> clazz) throws ServletException {
        List<Field> privateFields = ClassUtil.getPrivateFields(clazz);
        T form = null;
        try {
            form = clazz.newInstance();
            for(Field field: privateFields) {
                field.setAccessible(true);
                if (Long.TYPE.equals(field.getType())) {
                    field.setLong(form, Long.valueOf(req.getParameter(field.getName())));
                } else {
                    field.set(form, req.getParameter(field.getName()));
                }
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
        return form;
    }
}
