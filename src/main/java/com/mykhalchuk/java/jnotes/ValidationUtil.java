package com.mykhalchuk.java.jnotes;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ValidationUtil {
    public static List<String> validate(Object form) {
        List<String> errors = new ArrayList<>();

        Class<?> formClass = form.getClass();
        for (Field field: formClass.getDeclaredFields()) {
            NotEmpty notEmpty = field.getAnnotation(NotEmpty.class);
            if (notEmpty != null) {
                try {
                    field.setAccessible(true);
                    if (field.get(form).toString().isEmpty()) {
                        errors.add(notEmpty.value());
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        return errors;
    }
}
