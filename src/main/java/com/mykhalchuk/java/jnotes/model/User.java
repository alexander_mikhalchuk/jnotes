package com.mykhalchuk.java.jnotes.model;

public class User {
    private long id;
    private final String username;
    private final String passwordDigest;
    private final String fullName;

    public User(long id, String username, String passwordDigest, String fullName) {
        this.username = username;
        this.passwordDigest = passwordDigest;
        this.fullName = fullName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public String getPasswordDigest() {
        return passwordDigest;
    }

    public String getFullName() {
        return fullName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (!username.equals(user.username)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }
}
