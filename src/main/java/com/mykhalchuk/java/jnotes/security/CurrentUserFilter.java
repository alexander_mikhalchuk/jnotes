package com.mykhalchuk.java.jnotes.security;

import com.mykhalchuk.java.jnotes.HttpUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class CurrentUserFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String currentUser = (String) httpServletRequest.getSession().getAttribute("currentUser");

        httpServletRequest.setAttribute("userName", currentUser);
        httpServletRequest.setAttribute("isLoggedIn", HttpUtil.isLoggedIn(httpServletRequest));

        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}
