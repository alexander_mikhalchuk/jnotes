package com.mykhalchuk.java.jnotes.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordEncoder {
    private static final String PASSWORD_SALT = "dfhjdfhj3434jfhfDFDF";
    private static MessageDigest digest;
    static {
        try {
            digest = MessageDigest.getInstance("md5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String encode(String password) {
        try {
            return new String(digest.digest((password + PASSWORD_SALT).getBytes()), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 is not supported!!! WTF?", e);
        }
    }
}
