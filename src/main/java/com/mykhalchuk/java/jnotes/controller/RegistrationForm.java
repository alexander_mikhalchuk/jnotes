package com.mykhalchuk.java.jnotes.controller;

import com.mykhalchuk.java.jnotes.NotEmpty;

public class RegistrationForm {
    @NotEmpty("Username field cannot be blank.")
    private final String username;

    @NotEmpty("Password field cannot be blank.")
    private final String password;

    @NotEmpty("Full name field cannot be blank.")
    private final String fullName;

    public RegistrationForm() {
        this("", "", "");
    }

    public RegistrationForm(String username, String password, String fullName) {
        this.username = username.trim();
        this.password = password;
        this.fullName = fullName.trim();
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFullName() {
        return fullName;
    }
}
