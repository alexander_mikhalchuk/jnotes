package com.mykhalchuk.java.jnotes.controller;

import com.mykhalchuk.java.jnotes.BindUtil;
import com.mykhalchuk.java.jnotes.LoginManager;
import com.mykhalchuk.java.jnotes.ValidationUtil;
import com.mykhalchuk.java.jnotes.dao.UserDaoDbImpl;
import com.mykhalchuk.java.jnotes.model.User;
import com.mykhalchuk.java.jnotes.dao.UserDao;
import com.mykhalchuk.java.jnotes.security.PasswordEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(value = "/registration.html")
public class RegistrationServlet extends HttpServlet {
    private UserDao userDao;
    private LoginManager loginManager;

    public RegistrationServlet() {
        userDao = new UserDaoDbImpl();
        loginManager = new LoginManager();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/registration.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        RegistrationForm registrationForm = BindUtil.bind(req, RegistrationForm.class);
        List<String> errors = validate(registrationForm);
        if(errors.isEmpty()) {
            User user = new User(0L,
                    registrationForm.getUsername(),
                    PasswordEncoder.encode(registrationForm.getPassword()),
                    registrationForm.getFullName());
            userDao.create(user);

            loginManager.login(req, registrationForm.getUsername());
            res.sendRedirect("/index.html");
        } else {
            req.setAttribute("registrationForm", registrationForm);
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("/WEB-INF/templates/registration.jsp").forward(req, res);
        }
    }

    private List<String> validate(RegistrationForm registrationForm) {
        List<String> errors = ValidationUtil.validate(registrationForm);

        if(userDao.findBy(registrationForm.getUsername()) != null) {
            errors.add("User '" + registrationForm.getUsername() + "' already exists.");
        }

        return errors;
    }
}
