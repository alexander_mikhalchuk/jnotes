package com.mykhalchuk.java.jnotes.controller;

import com.mykhalchuk.java.jnotes.NotEmpty;

public class NoteForm {
    private long id;
    @NotEmpty("Note text cannot be blank")
    private String content;

    public NoteForm(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public NoteForm() {
        this(0L, "");
    }
}
