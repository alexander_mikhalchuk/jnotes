package com.mykhalchuk.java.jnotes.controller;

import com.mykhalchuk.java.jnotes.BindUtil;
import com.mykhalchuk.java.jnotes.ValidationUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/user/createNewNote.html")
public class CreateNewNoteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/createNewNote.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        NoteForm noteForm = BindUtil.bind(req, NoteForm.class);
        List<String> errors = ValidationUtil.validate(noteForm);

        if(errors.isEmpty()) {
            System.out.println("Save Note into DB...");

        } else {
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("/WEB-INF/templates/createNewNote.jsp").forward(req, resp);
        }
    }
}
