package com.mykhalchuk.java.jnotes.controller;

import com.mykhalchuk.java.jnotes.BindUtil;
import com.mykhalchuk.java.jnotes.LoginManager;
import com.mykhalchuk.java.jnotes.dao.UserDaoDbImpl;
import com.mykhalchuk.java.jnotes.model.User;
import com.mykhalchuk.java.jnotes.dao.UserDao;
import com.mykhalchuk.java.jnotes.security.PasswordEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login.html")
public class LoginServlet extends HttpServlet {
    private UserDao userDao;
    private LoginManager loginManager;

    public LoginServlet() {
        userDao = new UserDaoDbImpl();
        loginManager = new LoginManager();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LoginForm loginForm = BindUtil.bind(req, LoginForm.class);

        User user = userDao.findBy(loginForm.getUsername(),
                PasswordEncoder.encode(loginForm.getPassword()));
        if (user != null) {
            loginManager.login(req, user.getUsername());
            resp.sendRedirect("/index.html");
        } else {
            req.setAttribute("loginForm", loginForm);
            req.setAttribute("error", "User does not exist.");
            req.getRequestDispatcher("/WEB-INF/templates/login.jsp").forward(req, resp);
        }
    }
}
