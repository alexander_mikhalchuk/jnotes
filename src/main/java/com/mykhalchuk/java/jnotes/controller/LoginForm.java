package com.mykhalchuk.java.jnotes.controller;

public class LoginForm {
    private final String username;
    private final String password;

    public LoginForm() {
        this("", "");
    }

    public LoginForm(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
