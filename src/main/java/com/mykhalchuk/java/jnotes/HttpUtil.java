package com.mykhalchuk.java.jnotes;

import javax.servlet.http.HttpServletRequest;

public class HttpUtil {

    public static boolean isLoggedIn(HttpServletRequest request) {
        String currentUser = (String) request.getSession().getAttribute("currentUser");
        return (currentUser != null);
    }

}
