package com.mykhalchuk.java.jnotes;

import javax.servlet.http.HttpServletRequest;

public class LoginManager {
    public void login(HttpServletRequest req, String username) {
        req.getSession().setAttribute("currentUser", username);
    }
}
