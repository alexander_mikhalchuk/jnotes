package com.mykhalchuk.java.jnotes.dao;

import com.mykhalchuk.java.jnotes.model.User;

public interface UserDao {
    void create(User user);

    User findBy(String username, String password);

    User findBy(String username);
}
