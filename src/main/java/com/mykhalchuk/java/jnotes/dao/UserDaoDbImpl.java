package com.mykhalchuk.java.jnotes.dao;

import com.mykhalchuk.java.jnotes.model.User;

import java.sql.*;

public class UserDaoDbImpl implements UserDao {
    static {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(User user) {
        String query = "insert into users (username, passwordDigest, fullName) values (?, ?, ?)";
        System.out.println(query);

        try (Connection connection = getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(query);

            stmt.setString(1, user.getUsername());
            stmt.setString(2, user.getPasswordDigest());
            stmt.setString(3, user.getFullName());

            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public User findBy(String username, String passwordDigest) {
        return find("select * from users where username = ? and passwordDigest = ?",
                    username, passwordDigest);
    }

    @Override
    public User findBy(String username) {
        return find("select * from users where username = ?", username);
    }

    private User find(String selectQuery, Object... parameters) {
        System.out.println(selectQuery);

        User user = null;
        try (Connection connection = getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(selectQuery);

            for (int i = 0; i < parameters.length; i++) {
                stmt.setObject(i + 1, parameters[i]);
            }

            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                user = new User(resultSet.getLong("id"),
                        resultSet.getString("username"),
                        resultSet.getString("passwordDigest"),
                        resultSet.getString("fullName"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    private Connection getConnection() throws SQLException {
        String home = System.getenv("USERPROFILE");
        return DriverManager.getConnection("jdbc:h2:file:" + home + "/.jnotes/jnotes", "sa", "sa");
    }
}
